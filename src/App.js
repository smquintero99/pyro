import React, { useState, useEffect, useCallback, Fragment, useRef, createRef } from 'react'

import { Cell, Column, Table } from '@blueprintjs/table'
import { 
    Alignment, 
    Button, 
    FormGroup,
    InputGroup, 
    Navbar, 
    NumericInput,
    Menu, 
    MenuItem, 
    Popover, 
    Position,
    Switch as BoolSwitch
} from '@blueprintjs/core'

import {
    BrowserRouter as Router,
    Link,
    Route,
    Switch,
    useParams
} from 'react-router-dom'

import {
    BSON,
    RemoteMongoClient,
    Stitch
} from 'mongodb-stitch-browser-sdk'

import './App.css'

const numeric_enum = 'number'
const bool_enum = 'bool'

const collection = 'Models'
const FieldsCollection = 'Fields'
const client = Stitch.initializeDefaultAppClient('pyro-ylgqn')
const db = client.getServiceClient(RemoteMongoClient.factory, 'mongodb-atlas').db('Pyro')

const get_all = () => db.collection(collection).find({}, { limit: 100}).asArray().catch(console.log)
const post_one = new_doc => db.collection(collection).insertOne(new_doc).catch(console.log)
const put_one = (_id, new_doc) => db.collection(collection).updateOne(
    { _id: new BSON.ObjectID(_id)}, {$set: new_doc}, {upsert: true}
).catch(console.log)

const delete_one = _id => db.collection(collection).deleteOne({_id: new BSON.ObjectID(_id)}).catch(console.log)


const get_columns = () => db.collection(FieldsCollection).find({}, { limit: 100 }).asArray().catch(console.log)
const add_column = new_column => db.collection(FieldsCollection).insertOne({ 
    default: new_column.type === numeric_enum ? 0 : '',
    ...new_column
})

const put_column = (_id, new_coulmn) => db.collection(FieldsCollection).updateOne(
    { _id: new BSON.ObjectID(_id)}, {$set: new_coulmn}, {upsert: true}
).catch(console.log)

const delete_column = _id => db.collection(FieldsCollection).deleteOne({_id: new BSON.ObjectID(_id)}).catch(console.log)


const List = ({ values, setId, columns, addColumn, isColumn }) => {
    const [ selectedType, setType ] = useState()
    const [ newColumn, nameColumn ] = useState('')

    const cellRenderer = (idx, col) => <Cell align={Alignment.RIGHT}> 
        <Link to={`${isColumn ? '/columns' : ''}/get/${idx + 1}`} style={{float:'right'}} onClick={() => setId(idx+1)}>
            {
                columns[col].type === numeric_enum
                    ?
                        values && values[idx] && values[idx][columns[col].value] 
                            ? `$${values[idx][columns[col].value].toFixed(2)}` 
                            : (0).toFixed(2) 
                    :
                        values && values[idx] && values[idx][columns[col].value] 
                            ? values[idx][columns[col].value] : ''
            } 
        </Link>
    </Cell>

    return <Fragment>
        <div  align={Alignment.CENTER}>
            <h1 className='bp3-heading' style={{margin: '3rem'}}> Get All </h1>
        </div>

        <div style={{margin: '3rem'}}>
            <BoolSwitch 
                checked={ false }
                label="Enable Soft Delete"
                onChange={() => {}} 
                style={{width: '100%', textAlign: 'right'}}
            />
            <InputGroup 
                fill={false}
                value={newColumn}
                onChange={({target: { value }}) => nameColumn(value)}
                placeholder={'Add New Column'} 
                rightElement={
                    <Fragment>
                        <Popover 
                            content={
                                <Menu>
                                    <MenuItem text='Any' onClick={()=> setType()}/>
                                    <MenuItem text='Text' onClick={()=> setType('text')}/>
                                    <MenuItem text='Number' onClick={()=> setType(numeric_enum)}/>
                                </Menu>
                            } 
                            position={Position.RIGHT_TOP}
                        >
                            <Button text={`Type${selectedType ? `: ${selectedType}` : ''}`} minimal={true} style={{minWidth: 64}}/>
                        </Popover>
                        <Button 
                            text={`Add`} 
                            minimal={true} 
                            style={{marginLeft: 16, minWidth: 96}} 
                            disabled={!newColumn}
                            onClick={() => {
                                addColumn({value: newColumn, type: selectedType})
                                nameColumn('')
                            }}
                        />
                    </Fragment>
                }
            />

            <Table numRows={values.length}>
                { 
                    columns.map(({value: column}, idx) => 
                        <Column 
                            key={idx}
                            name={column} 
                            cellRenderer={cellRenderer} 
                            style={{textAlign:'center'}} align={Alignment.CENTER}
                        />
                    )
                }
            </Table>
        </div>
    </Fragment>
}

const Post = ({ post, columns }) => {

    const map_doc = () => columns.reduce((d, { value, type }) => ({...d, [value]: type === numeric_enum ? 0 : ''}), {})

    const [ newDoc, setDoc ] = useState(map_doc())
    useEffect(() => { setDoc(map_doc()) }, [columns])

    const inputRefs = useRef([])
    useEffect(() => { inputRefs.current = columns.map(() => createRef()) }, [columns])

    return <div  align={Alignment.CENTER}>
        <h1 className='bp3-heading' style={{margin: '3rem'}}> Post </h1>
        <div style={{width:'fit-content'}}>
            {
                columns.map(({value: column, type}, idx) => 
                    <FormGroup
                        key={ idx }
                        inline
                        label={ column }
                        labelFor="text-input"
                    >
                        {
                            (({
                                [numeric_enum]: <NumericInput 
                                                    id="text-input" 
                                                    placeholder={`Enter ${column}`} 
                                                    value={ newDoc[column] } 
                                                    onValueChange={v => setDoc({...newDoc, [column]: v})}
                                                    style={{ textAlign:'right' }}
                                                />,

                                [bool_enum]:    <BoolSwitch 
                                                    checked={ false }
                                                    label=""
                                                    onChange={() => {}} 
                                                />

                            })[type] ||     <InputGroup
                                                id={`post-input-${idx}`}
                                                inputRef={ inputRefs.current[idx] }
                                                placeholder={`Enter ${column}`} 
                                                defaultValue={ newDoc[column] } 
                                                onChange={({ target: { value: v }}) => setDoc({...newDoc, [column]: v})}
                                                style={{ textAlign: 'right', paddingRight: 20, width: 227 }}
                                            />

                            )
                        }
                    </FormGroup>
                )
            }
            <Button 
                onClick={() => {
                    post(newDoc)
                    setDoc(map_doc())
                    inputRefs.current.map(({current}) => current ? current.value = '' : null)
                }}
            > Add New </Button>
        </div>
    </div>
} 

const Get = ({ values, columns }) => {
    const { id } = useParams()

    return <div align={Alignment.CENTER}>
        <h1 className='bp3-heading' style={{margin: '3rem'}}> Get </h1>
        {
            id <= values.length
                ?        
                    <table className="bp3-html-table .modifier">
                        <thead>
                            <tr>
                                <th>Field</th>
                                <th>Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                columns.map(({ value: column, type }, idx) => 
                                    <tr key={idx}>
                                        <td>{ column }</td>
                                        <td style={{textAlign: 'right'}}> 
                                            {
                                                values[id - 1][column] !== undefined
                                                ?   values[id - 1][column]
                                                :   ({ [bool_enum]: 'False'})[type]
                                            } 
                                        </td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                :   null
        }
    </div>
}

const Put = ({ values, put, columns }) => {

    const { id } = useParams()
    const [ newDoc, setDoc ] = useState(columns.reduce((d, { value, type }) => ({...d, [value]: type === numeric_enum ? 0 : ''}), {}))

    useEffect(() => {
        values[id-1]
            ?   setDoc(columns.reduce((d, {value, type}) => ({...d, [value]: values[id - 1][value] }), {}))
            :   setDoc({})
    }, [columns, values])

    return <div align={Alignment.CENTER}>
        <h1 className='bp3-heading' style={{margin: '3rem'}}> Put </h1>
        {
            id <= values.length
                ?
                    <div style={{width:'fit-content'}}>
                        {
                            columns.map(({ value: column, type }, idx) => 
                                <FormGroup
                                    key={idx}
                                    inline
                                    label={ column }
                                    labelFor="text-input"
                                >
                                    {
                                        (({
                                            [numeric_enum]:   <NumericInput 
                                                                id="text-input" 
                                                                placeholder={`Enter ${column}`} 
                                                                style={{textAlign:'right'}}
                                                                value={ newDoc[column] }
                                                                onValueChange={v => setDoc({...newDoc, [column]: v})}
                                                            />,

                                            [bool_enum]:     <BoolSwitch 
                                                                checked={ false }
                                                                label=""
                                                                onChange={() => {}} 
                                                            />

                                        })[type] ||     <InputGroup
                                                            id="text-input" 
                                                            placeholder={`Enter ${column}`} 
                                                            style={{ textAlign: 'right', paddingRight: 20, width: 227 }}
                                                            defaultValue={ newDoc[column] }
                                                            onChange={({ target: {value: v}}) => setDoc({...newDoc, [column]: v})}
                                                        />
                                        )
                                    }
                                </FormGroup>
                            )
                        }
                        <Button onClick={ () => put({...values[id - 1], ...newDoc}) } > Submit </Button>
                    </div>
            :   null
        }
    </div>
}

const Delete = ({ values, remove, columns }) => {
    const { id } = useParams()

    return <div align={Alignment.CENTER}>
        <h1 className='bp3-heading' style={{margin: '3rem'}}> Delete </h1>
        {
            id <= values.length
                ?
                    <Fragment>
                        <table className="bp3-html-table .modifier">
                            <thead>
                                <tr>
                                    <th>Field</th>
                                    <th>Value</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    columns.map(({value: column, type}, idx) => 
                                        <tr key={idx}>
                                            <td>{column}</td>
                                            <td style={{textAlign: 'right'}}> 
                                                { 
                                                    values[id - 1][column] !== undefined
                                                    ?   values[id - 1][column]
                                                    :   ({ [bool_enum]: 'False'})[type]
                                                } 
                                            </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>

                        <Button
                            style={{marginTop:20}} 
                            onClick={ ()=> remove(id) } 
                        > Delete </Button>
                    </Fragment>

                :   null
        }
    </div>
}


export default function App() {

    const [ id, setId ] = useState(1)
    const [ values, setValues ] = useState([])
    const [ columns, setColumns ] = useState([])
    const [ isColumn, switch_isColumn ] = useState(false)

    const meta_columns = [
        { value: 'value', type:'text', default:'' }, 
        { value: 'type', type: 'text', default:'' }, 
        { value: 'default', type: 'text', default: '' },
        { value: 'created', type: bool_enum, default: false },
        { value: 'updated', type: bool_enum, default: false },
        { value: 'created_by', type: bool_enum, default: false },
        { value: 'updated_by', type: bool_enum, default: false },
        { value: 'active', type: bool_enum, default: false },
        { value: 'deleted_by', type: bool_enum, default: false },
        { value: 'deleted_on', type: bool_enum, default: false },
    ]


    useEffect(() => {
        async function fetchData(){
            const columns = (await get_columns()).map(({...doc, _id}) => ({...doc, _id: new BSON.ObjectID(_id).toHexString() }))
            setColumns(columns)
            const values = (await get_all()).map(({...doc, _id}) => ({...doc, _id: new BSON.ObjectID(_id).toHexString() }))
            setValues(values)
        }
        
        fetchData()
    }, [])

    const post_and_reload = useCallback(async(newDoc) => {
        await post_one(newDoc)
        const new_values = (await get_all()).map(({...doc, _id}) => ({...doc, _id: new BSON.ObjectID(_id).toHexString() }))
        setValues(new_values)
    }, [values])


    const put_and_reload = useCallback(async(newDoc) => {
        const {_id, ...doc} = newDoc
        await put_one(_id, doc)
        const new_values = (await get_all()).map(({...doc, _id}) => ({...doc, _id: new BSON.ObjectID(_id).toHexString() }))
        setValues(new_values)
    }, [values])

    const delete_and_reload = useCallback(async(id) => {
        await delete_one(values[id - 1]._id)
        const new_values = (await get_all()).map(({...doc, _id}) => ({...doc, _id: new BSON.ObjectID(_id).toHexString() }))
        setValues(new_values)

    }, [values])

    const add_column_and_reload = useCallback(async(value) => {
        await add_column(value)
        const columns = (await get_columns()).map(({...doc, _id}) => ({...doc, _id: new BSON.ObjectID(_id).toHexString() }))
        setColumns(columns)
    })

    const put_column_and_reload = useCallback(async(newDoc) => {
        const {_id, ...doc} = newDoc
        await put_column(_id, doc)
        const columns = (await get_columns()).map(({...doc, _id}) => ({...doc, _id: new BSON.ObjectID(_id).toHexString() }))
        setColumns(columns)
    })

    const delete_column_and_reload = useCallback(async(id) => {
        await delete_column(columns[id - 1]._id)
        const new_columns = (await get_columns()).map(({...doc, _id}) => ({...doc, _id: new BSON.ObjectID(_id).toHexString() }))
        setColumns(new_columns)
    })


    return <Router>
        <Navbar>
            <Navbar.Group align={Alignment.LEFT}>
                <Navbar.Heading>Pyro</Navbar.Heading>
                <Navbar.Divider />
                <Link to='/'>
                    <Button 
                        className='bp3-minimal' 
                        icon='home' 
                        text='Home' 
                        onClick={() => switch_isColumn(false)}
                    />
                </Link>
                <Link to='/columns'>
                    <Button 
                        className='bp3-minimal' 
                        text='Columns' 
                        onClick={() => switch_isColumn(true)}
                    />
                </Link>
            </Navbar.Group>

            <Navbar.Group align={Alignment.RIGHT}>
                <Link to={`${isColumn ? '/columns' : ''}/get/${id}`}>
                    <Button className='bp3-minimal' text='Get' />
                </Link>

                <Link to={`${isColumn ? '/columns' : ''}/post`}>
                    <Button className='bp3-minimal' text='Post' />
                </Link>

                <Link to={`${isColumn ? '/columns' : ''}/put/${id}`}>
                    <Button className='bp3-minimal' text='Put' />
                </Link>

                <Link to={`${isColumn ? '/columns' : ''}/delete/${id}`}>
                    <Button className='bp3-minimal' text='Delete' to='/delete' />
                </Link>
            </Navbar.Group>
        </Navbar>

        <Switch>
            <Route path='/columns/get/:id'>
                <Get 
                    id={ id }
                    values={ columns } 
                    columns={ meta_columns }
                />
            </Route>

            <Route path='/columns/post'>
                <Post 
                    columns={ meta_columns }
                    post={(newColumn) => add_column_and_reload(newColumn)}
                />
            </Route>

            <Route path='/columns/put/:id'>
                <Put 
                    values={ columns } 
                    columns={ meta_columns }
                    put={(newColumn) => put_column_and_reload(newColumn)}
                />
            </Route>

            <Route path='/columns/delete/:id'>
                <Delete 
                    values={ columns } 
                    columns={ meta_columns }
                    remove={(id) => delete_column_and_reload(id)}
                />
            </Route>

            <Route path='/columns'>
                <List 
                    values={ columns }
                    setId={(id) => setId(id)} 
                    columns={ meta_columns }
                    addColumn={() => {}}
                    isColumn={ true }
                />
            </Route>

            <Route path='/get/:id'>
                <Get 
                    id={ id }
                    values={ values } 
                    columns={ columns }
                />
            </Route>

            <Route path='/post'>
                <Post 
                    columns={ columns }
                    post={(newDoc) => post_and_reload(newDoc)}
                />
            </Route>

            <Route path='/put/:id'>
                <Put 
                    values={ values } 
                    columns={ columns }
                    put={(newDoc) => put_and_reload(newDoc)}
                />
            </Route>

            <Route path='/delete/:id'>
                <Delete 
                    values={ values } 
                    columns={ columns }
                    remove={(id) => delete_and_reload(id)}
                />
            </Route>

            <Route path='/'>
                <List 
                    values={ values } 
                    setId={(id) => setId(id)} 
                    columns={ columns }
                    addColumn={(new_column) => add_column_and_reload(new_column)}
                />
            </Route>
        </Switch>
    </Router>
}
