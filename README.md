
# CRUD CLI

### Description: 
Open Source Project to create reusable full stack templates for the most common operations to query a DB driven application.

### Benefits:
  - Knowledge:
  - TypeScript
  - Angular2
  - Bash/ Shell, Python Kotlin.
  - GraphQL / Serverless DB
  - Text Generation Technologies
  - WebFlow and New Generation CMS
  - Unit Testing


### Financial: 
Positional myself as a passionate, selfless contributor that can recognize abstractions and automatize work. Good potential, to create a factory and serve multiple business applications through it.

### Positioning strategy: 
Each epic would go accompanied with an article, each feature with a deployed functional project. Strategic posting to Linkedin.

### Potential: 
After the foundation it can be accompanied with Machine Learning, Alert & Notification Systems, support to multiple DBs, JS and back-end frameworks.

### Evaluation: 
It is a good project in the sense that it is not too challenging, will contribute to my employability and creates opportunities to share with close acquaintances creating wealth in the process.

### Score: 
  - Popular: 		5/10 it’s safe to say 1 in every 2 business could benefit from the project,
  - Growing; 		8/10 SAAS is increasing far greater from available talent.
  - Urgent: 		6/10 as an imminent crisis is approaching.
  - Expensive: 	7/10 even for a cost reduction product, is expensive.  
  - Mandatory: 	4/10 although only economic reasons are in place.
  - Frequent: 		,9/10 it would be used daily and even several times a day,



## ROADMAP:


#### CRUD Model <Name>:

  - Methods to POST, PUT, DELETE, GET & GET ALL records.
  - Forms for POST & PUT, Link and Confirmation for DELETE, Card for GET and Table for GET ALL.
  - Charts, Pie & Bar for Categorical. Line for Historic & Scatter for Numeric.
  - Data Labeling for Machine Learning & Predictions.
  - Alerts for Anomalies.
  - Clustering for Items.
  - Maps for Geographic Data.
  - Explore Foreign Keys,


#### Login App <Name>

  - Create Tables
  - Add OAuth
  - Allow Roles
  - Add DB Permissions
  - Add Template

